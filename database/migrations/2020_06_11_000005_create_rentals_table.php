<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rentals')) {
            Schema::create('rentals', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->date('start_date');
                $table->date('end_date');
                $table->float('penalty_amount',4,2)->nullable();
                $table->boolean('returned')->nullable();
                $table->integer('qty');
                $table->float('total_amount',4,2);
                $table->timestamps();
                //FK
                $table->unsignedBigInteger('client_id');
                $table->unsignedBigInteger('movie_id');
                $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
    }
}
