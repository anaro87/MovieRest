<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeByMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('like_by_movies')) {
            Schema::create('like_by_movies', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                //FK
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('movie_id');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('movie_id')->references('id')->on('movies')->onDelete('cascade');
            });
       } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_by_movies');
    }
}
