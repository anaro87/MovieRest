<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeByMovie extends Model
{
    protected $fillable = [
        'user_id', 'movie_id'
    ];
}
