<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Movie;
use App\User;
use App\Purchase;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
    * @param Request $request
	* @return \Illuminate\Http\JsonResponse
	* @throws \Illuminate\Validation\ValidationException
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: POST purchase a movie 
	*/
    public function store(Request $request)
    {
       
        try {
            
              // dd($request->all());
            $isValid = $request->validate([
                'qty' => 'required|integer',
                'client_id' => 'required',
                'movie_id' => 'required'
            ]);

            $movie = Movie::find($request->movie_id);
            $totalAmount = $request->qty * $movie->sale_price;
           
            $purchase = new Purchase();
            $purchase->total_amount =  $totalAmount;
            $purchase->qty = $request->qty;
            $purchase->client_id = $request->client_id;
            $purchase->movie_id = $request->movie_id;
            $purchase->created_at = now();
            $insert = $purchase->save();

            if ($insert) {
                return response()->json([
                    'success' => true,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, purchase could not be added'
                ], 500);
            }
            
        } catch (Exception $e) {
            var_dump($e);
            
        }
    }

    /**
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    * @throws \Illuminate\Validation\ValidationException
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description:get all 
    */
    public function index()
    {

        $results = DB::table('purchases')->get();

        return $results->toArray();

    }

    /**
    * @param $id
    * @return \Illuminate\Http\JsonResponse
    * @createdBy anaro87
    * @createdAt 6/13/2020
    * @Description: GET by id
    */
    public function show($id)
    {
        $purchase = Purchase::find($id);
    
        if (!$purchase) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, purchase with id ' . $id . 'was not be found'
            ], 400);
        }
    
        return $purchase;
    }
}
