<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Movie;
use App\User;
use App\LikeByMovie;

class LikeByMovieController extends Controller
{
    /**
    * @param Request $request
	* @return \Illuminate\Http\JsonResponse
	* @throws \Illuminate\Validation\ValidationException
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: like a movie 
	*/
    public function store(Request $request)
    {
       
        $authUser = Auth::user();
        $userId = $authUser->id;

        try {
            
              // dd($request->all());
            $isValid = $request->validate([
                'user_id' => 'required',
                'movie_id' => 'required'
            ]);
           
            $likeByMovie = new LikeByMovie();
            $likeByMovie->user_id = $userId;
            $likeByMovie->movie_id = $request->movie_id;
            $likeByMovie->created_at = now();
            $insert = $likeByMovie->save();

            if ($insert) {
                return response()->json([
                    'success' => true,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Sorry, like could not be added'
                ], 500);
            }
            
        } catch (Exception $e) {
            var_dump($e);
            
        }
    }

}
