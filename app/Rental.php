<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rental extends Model
{
    protected $fillable = [
        'start_date', 'end_date', 'penalty_amount', 'returned', 'qty', 'total_amount'
    ];

    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
    * @return mixed
	* @createdBy anaro87
	* @createdAt 6/13/2020
	* @Description: calculates penalty amount
	*/
    public function calculatePenalty($endDate, $penalty)
    {
    	$result = 0;
		$datework = Carbon::createFromDate($endDate);
		$now = Carbon::now();
		$days = $now->diffInDays($datework);

		if ($now->greaterThan($datework)) {
			$result = $days * $penalty;
		}
		return $result;



    }
}
