<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group( ['middleware' => 'auth.jwt',
			  // 'namespace' => 'App\Http\Controllers',
    	// 	  'prefix' => 'auth'
    		],  
    		function () {
			    Route::get('logout', 'ApiController@logout');
			    Route::get('user', 'ApiController@getAuthUser');

			    //movie
			    Route::get('movies', 'MovieController@index');
			    Route::get('movies/{id}', 'MovieController@show');
			    Route::post('movies', 'MovieController@store');
			    Route::put('movies/{id}', 'MovieController@update');
			    Route::patch('movies/{id}', 'MovieController@remove');
			    Route::delete('movies/{id}', 'MovieController@destroy');

			    //rentals
			    Route::get('rentals', 'RentalController@index');//get history
			    Route::post('rentals', 'RentalController@store');//rent a movie
			    Route::patch('rentals/{id}', 'RentalController@setPenalty');

			    //purchases
			    Route::get('purchases', 'PurchaseController@index');//get history
			    Route::patch('purchases/{id}', 'PurchaseController@show');
			    Route::post('purchases', 'PurchaseController@store');

			    //likes
				Route::post('like', 'LikeByMovieController@store');

			}
);